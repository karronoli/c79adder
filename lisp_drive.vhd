-- $Id$
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.my_txt_util.all;

entity lisp_drive is
end lisp_drive;

architecture behav of lisp_drive is
  signal CLK : std_logic := '0';  
  signal CODE_IN : string(1 to 1) := " ";
  signal CODE_OUT : string(1 to 1) := " ";
  signal work_address : std_logic_vector(7 downto 0) := (others => '0');
  signal char : std_logic_vector(7 downto 0) := (others => '0');
  signal result : std_logic_vector(7 downto 0) := (others => '0');
  component LISP
      port(
       CLK: in std_logic;
       CODE_IN: in string(1 to 1);
       CODE_OUT: OUT string(1 to 1);
       work_address: out std_logic_vector(7 downto 0);
       result: out std_logic_vector(7 downto 0)
       );
      
  end component;


begin
  char <= to_std_logic_vector(code_out);
  g0 : LISP port map (
    CLK=>CLK, CODE_IN=>CODE_IN, CODE_OUT=>CODE_OUT, work_address=>work_address, result=>result);
  
  process
    
  begin

      CLK <= '0'; wait for 1 ns;
      
      CLK <= '1'; CODE_IN <= "(";
      wait for 1 ns; CLK <= '0'; wait for 1 ns;

      --CLK <= '1'; CODE_IN <= " ";
      --wait for 1 ns; CLK <= '0'; wait for 1 ns;
      
      CLK <= '1'; CODE_IN <= "+";
      wait for 1 ns; CLK <= '0'; wait for 1 ns;
      
      CLK <= '1'; CODE_IN <= " ";
      wait for 1 ns; CLK <= '0'; wait for 1 ns;
      
      CLK <= '1'; CODE_IN <= "1";
      wait for 1 ns; CLK <= '0'; wait for 1 ns;
      assert not (1 = TO_INTEGER(unsigned(result))) report "input 1 OK" severity note;
      
      CLK <= '1'; CODE_IN <= " ";
      wait for 1 ns; CLK <= '0'; wait for 1 ns;
      
      CLK <= '1'; CODE_IN <= "2";
      wait for 1 ns;CLK <= '0'; wait for 1 ns;
      assert not (2 = TO_INTEGER(unsigned(result))) report "input 2 OK" severity note;
      
      CLK <= '1'; CODE_IN <= " ";
      wait for 1 ns;CLK <= '0'; wait for 1 ns;
      
      CLK <= '1'; CODE_IN <= ")";
      wait for 1 ns;CLK <= '0'; wait for 1 ns;
      assert not (3 = TO_INTEGER(unsigned(result))) report "add result 3 OK" severity note; 

      --CLK <= '1'; CODE_IN <= "_";
      --wait for 1 ns;CLK <= '0'; wait for 1 ns;

      assert false report "end of test" severity note;

    wait;
  end process;
end behav;
