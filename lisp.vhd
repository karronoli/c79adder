-- $Id$
library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.my_txt_util.all;

entity LISP is
  port(
    CLK: in std_logic;
    CODE_IN: in string(1 to 1);
    CODE_OUT: out string(1 to 1);
    work_address: out std_logic_vector(7 downto 0);
    result: out std_logic_vector(7 downto 0)
    );
end LISP;

architecture RTL of LISP is
  
  constant var_type_empty  : std_logic_vector(3 downto 0) := "0000";
  constant var_type_number : std_logic_vector(3 downto 0) := "0001";
  constant var_type_string : std_logic_vector(3 downto 0) := "0010";
  constant var_type_uninitialized : std_logic_vector(3 downto 0) := "0011";
  constant var_type_std_logic_vector : std_logic_vector(3 downto 0) := "0100";
  constant var_type_opcode : std_logic_vector(3 downto 0) := "0101";
  
  type variable_info is record
    var_type   :  std_logic_vector(3 downto 0);
    var_name : std_logic_vector(15 downto 0);
    var_value : std_logic_vector(7 downto 0);
  end record;
  type variable_array is array (Natural range <>) of variable_info;
  type vc_stack_t is array (natural range <>) of integer;
  type slv_stack_t is array (natural range <>) of std_logic_vector(7 downto 0);
  type STATE_T is (DISABLE, ENABLE, WAITING);

  signal STATE: STATE_T := ENABLE;
  signal memory : variable_array(255 downto 0) := (others=>(others=>(others=>'0')));
  signal vc_stack : vc_stack_t(3 downto 0) := (others=>0);
  signal opecode_stack : slv_stack_t(3 downto 0) := (others => (others=>'0'));
  signal w_address : unsigned(7 downto 0) := (others => '0');
  signal result_reg : std_logic_vector(7 downto 0) := (others => '0');
  signal opecode : std_logic_vector(7 downto 0) := (others => '0');
  signal user_variable : variable_info := (others => (others => '0'));
  signal input_char_sig : std_logic_vector(7 downto 0) := (others => '0');
  
begin
  result <= result_reg;
    
  main: process(CLK, CODE_IN)
    variable wm : variable_array(7 downto 0);
    variable w : variable_info := (others => (others => '0'));
    variable w_address_int : integer range 0 to 255 := 0;
    variable PC : integer range 0 to 15 := 0;
    variable VC : integer range 0 to 15 := 0;
    variable in_reg : std_logic_vector(7 downto 0) := (others => '0');
    variable opecode_address : integer range 0 to 255 := 0;
    variable opecode_address_tmp : unsigned(7 downto 0) := (others => '0');
    variable operand_address_tmp : unsigned(7 downto 0) := (others => '0');
    variable return_value_address : unsigned(7 downto 0) := (others => '0');
    variable unsigned_tmp : unsigned(3 downto 0) := (others => '0');
    variable input_char_slv : std_logic_vector(7 downto 0) := (others => '0');
  begin

    if rising_edge(CLK) then
      w_address_int := to_integer(w_address);
      case STATE is
        when WAITING => input_char_slv := memory(w_address_int).var_value;
        when others  => input_char_slv := to_std_logic_vector(CODE_IN);
      end case;
      input_char_sig <= input_char_slv;
      PC := to_integer(w_address(7 downto 4));
      VC := to_integer(w_address(3 downto 0));
      w := (others => (others => '0'));
      in_reg := input_char_slv;
      work_address <= std_logic_vector(w_address);
      code_out <= CODE_IN;

      case input_char_slv is
        when X"2B"| X"64" | X"67" | X"69" | X"73" | X"63" =>
        --when "+" | "i" | "d" | "s" | "g" | "c" =>
          w.var_type := var_type_opcode;
          w.var_value := in_reg;
          result_reg <= in_reg;
          memory(w_address_int) <= w;
          opecode <= in_reg;
          opecode_stack(PC) <= in_reg;

        when X"30" | X"31" | X"32" | X"33" | X"34" | X"35" | X"36" | X"37" | X"38" | X"39" =>
        --when "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" =>
          -- If found digit, captures as decimal number
          w.var_type := var_type_number;
          w.var_value := std_logic_vector(
            (unsigned(w.var_value) sll 3) +   
            (unsigned(w.var_value) sll 1) +  
            unsigned(in_reg) - 48  -- ASCII 0 = 48(dec)
            );
          
          result_reg <= w.var_value;
          memory(w_address_int) <= w;

        
        when X"20" =>   --when " " =>
          w_address(3 downto 0) <= w_address(3 downto 0) + 1;
          
        when X"28" => --when "(" =>
          vc_stack(PC) <= VC;
          w_address(7 downto 4) <= w_address(7 downto 4) + 1;  -- PC
          w_address(3 downto 0) <= (others => '0');  -- VC
          
        when X"29" => --when ")" =>
        
          case opecode is
            -- If first sign "+" of the elements, adding elements later
            when X"2B" =>               -- "+" =>
              for j in 1 to VC loop
                w.var_value := std_logic_vector(unsigned(w.var_value) +
                                                unsigned(memory(PC * 16 + j).var_value));
              end loop;
              w.var_type := var_type_number;

            when X"64" =>               -- "d" => -- define
              w := memory(PC * 16 + 1);
              user_variable <= w;
                            
            when X"67" =>               -- "g" => -- goto
              w_address <= unsigned(memory(PC * 16 + 1).var_value);
              
            when X"69" =>               -- "i" => -- if ... failed...
              if unsigned(user_variable.var_value) = 0 then
                w.var_value := (others => '1');
                STATE <= ENABLE;
              else
                w_address <= unsigned(memory((PC-1) * 16 + 0).var_value);
                STATE <= WAITING;
              end if;      
              
            when X"73" =>               -- "s" => -- succ =  user_variable + 1
              user_variable.var_value <= std_logic_vector(unsigned(user_variable.var_value) + 1);

            when X"63" =>               -- "c" => -- ccus(invert succ) =  user_variable - 1
              user_variable.var_value <= std_logic_vector(unsigned(user_variable.var_value) - 1);
              
            when others => null;
          end case;
          result_reg <= w.var_value;
          memory((PC-1)*16 + vc_stack(PC-1) + 1) <= w;
          w_address(7 downto 4) <= w_address(7 downto 4) - 1;  -- PC
          w_address(3 downto 0) <= unsigned(conv_std_logic_vector(vc_stack(PC-1) + 1, 4));  -- VC
          opecode <= opecode_stack(PC-1);

        when others => null;
      end case;
    end if;
  end process;
  

end RTL;


