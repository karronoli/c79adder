REVISION = "$Id$"

from myhdl import Signal, delay, always, now, Simulation, intbv, concat
import re

# EMPTY, NUMBER, STRING, OPCODE = range(4)
EMPTY, NUMBER, STRING, OPCODE = [intbv(x) for x in range(4)]
def ClkDriver(clk):
    halfPeriod = delay(10)
    @always(halfPeriod)
    def driveClk():
        clk.next = not clk
    return driveClk

def HelloWorld(clk, code_in):
    
    @always(clk.posedge)
    def sayHello():
        print "%s Hello World!" % now()
        print '%s code in' % code_in
    
    mc = {'type':EMPTY, 'name':EMPTY, 'value':EMPTY}
    wc = {'type':EMPTY, 'name':EMPTY, 'value':EMPTY}
    memory = {}
    work = {}
    pc = intbv(0)[4:]
    vc = intbv(0)[4:]
    vc_stack = {}

    @always(clk.posedge)
    def main():
        result = intbv(0)[8:]
        work_address = concat(pc, vc)
        p = re.compile("\d")

        if code_in == '+':
            wc['type'] = OPCODE
            wc['name'] = 'test'
            wc['value'] = code_in
            work[work_address.value] = wc

        elif p.match(code_in):
            wc['type'] = NUMBER
            wc['name'] = 'test'
            wc['value'] = wc['value'] + ord(code_in) - 48
            work[work_address.value] = wc
            
        elif code_in == ' ':
            memory[work_address.value] = work[work_address.value]
            vc += 1
            
        elif code_in == '(':
            vc_stack[pc.value] == vc
            vc = intbv(0)[4:]
            pc += 1
            
        elif code_in == ')':
            current_opecode = intbv(0)[8:]
            current_opecode[8:4] = pc
            print "current opecode %s" % work[current_opecode.value]['value'].value
            if work[current_opecode.value]['value'].value == ord('+'):
                pass
        else:
            pass

            
        return result, work_address
        
    return sayHello, main

if __name__ == '__main__':
    clk = Signal(0)
    clkdriver_inst = ClkDriver(clk = clk)
    hello_inst = HelloWorld(clk, '(')
    sim = Simulation(clkdriver_inst, hello_inst)
    sim.run(50)
