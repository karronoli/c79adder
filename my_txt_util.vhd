-- $Id$
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package my_txt_util is

    function to_std_logic_vector(c: character) return std_logic_vector;
    ---- converts a string into std_logic_vector
    function to_std_logic_vector(s: string) return std_logic_vector;

    function CONV_STD_LOGIC_VECTOR(ARG: INTEGER; SIZE: INTEGER) return STD_LOGIC_VECTOR;
end my_txt_util;

package body my_txt_util is

-- converts a string into std_logic_vector

--function to_std_logic_vector(c: character) return variable_info is
function to_std_logic_vector(c: character) return std_logic_vector is
begin
  return CONV_STD_LOGIC_VECTOR(character'pos(c), 8);
end to_std_logic_vector;

function to_std_logic_vector(s: string) return std_logic_vector is
  variable slv: std_logic_vector((s'length * 8) - 1 downto 0);
  variable index : integer := 0;
begin
  for i in 1 to s'length loop
    index:=i*8;
    slv(index-1 downto index-8) := to_std_logic_vector(s(i));
  end loop; 
  return slv;
    
end to_std_logic_vector;


function CONV_STD_LOGIC_VECTOR(ARG: INTEGER; SIZE: INTEGER) return STD_LOGIC_VECTOR is
	variable result: STD_LOGIC_VECTOR (SIZE-1 downto 0);
	variable temp: integer;
	-- synopsys built_in SYN_INTEGER_TO_SIGNED
	-- synopsys subpgm_id 381
    begin
	-- synopsys synthesis_off
	temp := ARG;
	for i in 0 to SIZE-1 loop
	    if (temp mod 2) = 1 then
		result(i) := '1';
	    else 
		result(i) := '0';
	    end if;
	    if temp > 0 then
		temp := temp / 2;
	    elsif (temp > integer'low) then
		temp := (temp - 1) / 2; -- simulate ASR
	    else
		temp := temp / 2; -- simulate ASR
	    end if;
	end loop;
	-- synopsys synthesis_on
	return result;
end CONV_STD_LOGIC_VECTOR;


end my_txt_util;
