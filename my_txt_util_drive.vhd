library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.my_txt_util.all;

entity my_txt_util_drive is
end my_txt_util_drive;

architecture behav of my_txt_util_drive is

  signal CI : std_logic := '0';
  signal in1 : character := 'A';
  signal in2 : string(2 downto 1) := "BC";
  signal in3 : integer := 3;
  signal in4 : character := '+';
  signal in5 : string(7 downto 1) := "(+ 1 2)";
  signal out1: std_logic_vector(7 downto 0) := "01000001";
  signal out2: std_logic_vector(15 downto 0) := "0100001001000011";
  signal out3: std_logic_vector(3 downto 0) := "0011";
  signal out4 : std_logic_vector(7 downto 0) := "00101011";
  signal result1 : std_logic_vector(7 downto 0) := (others => '0');
  signal result2 : std_logic_vector(15 downto 0) := (others => '0');
  signal result3 : std_logic_vector(3 downto 0) := (others => '0');
  signal result4 : std_logic_vector(7 downto 0) := (others => '0');
  signal result5 : std_logic_vector(55 downto 0) := (others => '0');
begin
  
  process
    
  begin

      assert (in2'length=2) report "test" severity error;
      
      result1 <= to_std_logic_vector(in1); wait for 1 ns;
      assert result1 = out1 report "charcter NG" severity error;
      --result2 <= to_std_logic_vector("B"); wait for 1 ns;
      result2 <= to_std_logic_vector(in2); wait for 1 ns;
      assert result2 = out2 report "string not valid" severity error;
      result3 <= CONV_STD_LOGIC_VECTOR(in3, 4); wait for 1 ns;
      assert result3 = out3 report "cslv is not valid" severity error;
      result4 <= to_std_logic_vector(in4); wait for 1 ns;
      assert result4 = out4 report "+ is not valid" severity error;
      result5 <= to_std_logic_vector(in5);  wait for 1 ns;
      
    assert false report "end of test" severity note;

    wait;
  end process;
end behav;

